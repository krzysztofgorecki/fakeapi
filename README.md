# Fake api
Sometimes you need to use some fake api and for some sercurity reasons you cannot or don't want to waste time on using ready made solutions. This project was originated by exactly that kind of situation.

## Installing
mvn clean install

## Running
java -jar target/fakeapi-{version}.jar

Hosted on GitHub, GitLab and Bitbucket - one of those will (should?) work in your organisation.
